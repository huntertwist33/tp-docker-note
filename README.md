# 1 - Conteneurisation de l’application 

## BUILD IMAGE PHP :

docker build -t gestion-produits-php -f Dockerfile-php .  

## BUILD IMAGE MySQL

docker build -t gestion-produits-mysql -f Dockerfile-mysql .  

## CRÉATION D’UN RÉSEAU DOCKER :

docker network create reseau_docker

## RUN IMAGE PHP :

docker run -it --name PHP_5.0 --network reseau_docker -p 8080:80  gestion-produits-php

## RUN IMAGE MYSQL :

docker run -d -it --name MySQL_5.7 --network reseau_docker -p 33060:3306 -v mysql-data:/var/lib/mysql -e MYSQL_ROOT_PASSWORD=password  gestion-produits-mysql

# 2 - Mise en place de Docker Compose 

## Commande pour lancer le Docker Compose : 

docker-compose up

# 3 - Version de dev : mise à jour de la plate-forme

## Se déplacer dans la branche "8.3", récupérer le contenu et lire le "README" de la branche pour cette partie du TP

# 4 - Branche PostgreSQL

## Se déplacer dans la branche "PostgreSQL", récupérer le contenu et lire le "README" de la branche pour cette partie du TP
